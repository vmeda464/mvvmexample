//
//  APIClient.swift
//  TopMovies_MVVM
//
//  Created by Venkateswara Meda on 27/11/18.
//  Copyright © 2018 Venkateswara Meda. All rights reserved.
//

import Foundation

class APIClient: NSObject {
    
    func fetchMovies(completion:@escaping (MainFeed?, Error?) -> ()) {
        let urlString = "https://itunes.apple.com/us/rss/topmovies/limit=150/json"
        guard let url = URL(string: urlString) else {
            return
        }
        
        let session = URLSession(configuration: .default)
        _ = session.dataTask(with: url, completionHandler: { (data, response, error) in
            
            guard let data  = data else {
                return
            }
            
            do{
                let mainFeed = try JSONDecoder().decode(MainFeed.self, from: data)

                completion(mainFeed, nil)
                
            } catch {
                completion(nil, error)
            }
            
        }).resume()
    }
}

//
//  ViewController.swift
//  TopMovies_MVVM
//
//  Created by Venkateswara Meda on 27/11/18.
//  Copyright © 2018 Venkateswara Meda. All rights reserved.
//

import UIKit

protocol MainViewDataSource: class {
    func setNumberOfRowsInSection(section:Int) -> Int
    func titleForRowAtIndexpath(indexPath:IndexPath) -> String
}

class MainView: UIView {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityLoader: UIActivityIndicatorView!
    weak var feedDataSource: MainViewDataSource!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        activityLoader.color = UIColor.green
        activityLoader.hidesWhenStopped = true
        activityLoader.startAnimating()
        tableView.dataSource = self
        
    }
}

extension MainView: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (feedDataSource.setNumberOfRowsInSection(section: section))
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        self.configureCell(cell: cell, forRowAtIndexPath: indexPath)
        return cell
    }
    
    func configureCell(cell:UITableViewCell, forRowAtIndexPath indexpath:IndexPath) {
        let name = feedDataSource.titleForRowAtIndexpath(indexPath: indexpath)
        cell.textLabel?.text = name
    }
}

extension MainView: UITableViewDataSourcePrefetching {
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        print("prefetchRowsAt \(indexPaths)")
    }
    
    func tableView(_ tableView: UITableView, cancelPrefetchingForRowsAt indexPaths: [IndexPath]) {
        print("cancelPrefetchingForRowsAt \(indexPaths)")
    }
}

extension MainView: ViewModelDelegate {
    
    func moviesArrayUpdated() {
        DispatchQueue.main.async {
            self.activityLoader.stopAnimating()
            self.tableView.reloadData()
        }
    }
}

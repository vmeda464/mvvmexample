//
//  ViewController.swift
//  TopMovies_MVVM
//
//  Created by Venkateswara Meda on 27/11/18.
//  Copyright © 2018 Venkateswara Meda. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var viewModel:ViewModel!
    @IBOutlet weak var mainView: MainView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainView.feedDataSource = viewModel
        viewModel.modelDelegate = mainView
        viewModel.featchMoviesList()
    }
}


//
//  ViewModel.swift
//  TopMovies_MVVM
//
//  Created by Venkateswara Meda on 27/11/18.
//  Copyright © 2018 Venkateswara Meda. All rights reserved.
//

import UIKit

protocol ViewModelDelegate: class {
    func moviesArrayUpdated()
}

class ViewModel: NSObject {

    var moviesArray: MainFeed?
    weak var modelDelegate: ViewModelDelegate!
    
    @IBOutlet var apiClient: APIClient!
    
    func featchMoviesList() {
        apiClient.fetchMovies { (feeds, error) in
            guard let listOfMovies = feeds else {
                return
            }
            self.moviesArray = listOfMovies
            self.modelDelegate.moviesArrayUpdated()
        }
    }
   
}

extension ViewModel: MainViewDataSource {
    func setNumberOfRowsInSection(section:Int) -> Int {
        return self.moviesArray?.feed?.entry?.count ?? 0
    }
       
    func titleForRowAtIndexpath(indexPath:IndexPath) -> String {
        return self.moviesArray?.feed?.entry?[indexPath.row].imName?.label ?? "N/A"
    }
}
